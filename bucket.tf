locals {
  website_folder = var.website_folder != null ? var.website_folder : "${path.root}/website"
}

resource "aws_s3_bucket" "s3" {
  bucket = var.bucket_name
}

module "template_files" {
  source   = "hashicorp/dir/template"
  base_dir = local.website_folder
}

resource "aws_s3_object" "s3_files" {
  bucket       = aws_s3_bucket.s3.id
  for_each     = module.template_files.files

  key          = each.key
  content_type = each.value.content_type
  source       = each.value.source_path
  etag         = each.value.digests.md5
}