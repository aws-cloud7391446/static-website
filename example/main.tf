module "static_website" {
    source               = "../"

    bucket_name          = "static-website-example"
    root_document_object = null
    website_folder       = null
    cache_policy         = null
    domain               = null
    api_backend          = null

    providers = {
      aws.virginia = aws.virginia
    }
}