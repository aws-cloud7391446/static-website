output "url" {
  description = "Static website domain name"
  value       = module.static_website.url
}