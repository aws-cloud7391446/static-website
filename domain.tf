locals {
  use_domain       = var.domain != null
  use_domain_alias = (!local.use_domain ? false :
    var.domain.type == "A"
  )
  use_domain_cname = (!local.use_domain ? false :
    var.domain.type == "CNAME"
  )
}

data "aws_route53_zone" "primary" {
  name  = var.domain.zone

  count = local.use_domain ? 1 : 0
}

resource "aws_route53_record" "website_alias" {
  zone_id = data.aws_route53_zone.primary[0].zone_id
  name    = var.domain.name
  type    = "A"

  count   = local.use_domain_alias ? 1 : 0

  alias {
    name                   = aws_cloudfront_distribution.s3_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.s3_distribution.hosted_zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "website_cname" {
  zone_id = data.aws_route53_zone.primary[0].zone_id
  name    = replace(var.domain.name, ".${var.domain.zone}", "")
  type    = "CNAME"
  ttl     = var.domain.ttl

  count   = local.use_domain_cname ? 1 : 0

  records = [aws_cloudfront_distribution.s3_distribution.domain_name]
}