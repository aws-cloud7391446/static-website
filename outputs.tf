output "url" {
  description = "Static website domain name"
  value       = local.use_domain ? var.domain.name : aws_cloudfront_distribution.s3_distribution.domain_name
}