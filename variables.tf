variable "bucket_name" {
  type        = string
  description = "(Required) S3 bucket name for a website"
}

variable "root_document_object" {
  type        = string
  default     = "index.html"
  description = "A root document object of a website. 'index.html' by default"
  nullable    = false
}

variable "website_folder" {
  type        = string
  default     = null
  description = "A folder with website static files. Default folder is 'website' in the root directory"
}

variable "cache_policy" {
  type        = string
  default     = "optimized"
  description = "Cache policy to use with CloudFront. Must be one of 'optimized', 'disabled'. 'optimized' by default"
  nullable    = false
}

variable "domain" {
  type = object({
    name = string
    type = string
    cert = string
    zone = string
    ttl  = number
  })
  default     = null
  description = <<-EOT
    A domain name for a website. It must be managed by a hosted domain zone and have a TLS certificate in the us-east-1 region
      name - Domain name for a website
      type - Domain record type. Must be 'A' or 'CNAME'
      cert - TLS certificate domain name
      zone - Hosted domain zone name
      ttl  - TTL in seconds for record (CNAME only)
  EOT
}

variable "api_backend" {
  type = object({
    origin     = string
    http_port  = number
    https_port = number
  })
  default     = null
  description = <<-EOT
    API Backend object for CloudFront, HTTPS only
      origin     - Domain name and path to backend
      http_port  - HTTP port
      https_port - HTTPS port
  EOT
}