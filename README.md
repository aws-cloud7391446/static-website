# Static Website
Terraform application that allows you to quickly set up and deploy a serverless files hosting
with an optional HTTPS API integration or domain name with TLS certificate  
You can use it for a website or some kind of storage

## Architecture
![Schema](/uploads/3de5255fc82230312bf6a04824ee0aea/schema.png)

Files are uploaded and stored in the S3 bucket where accessed and cached via the CloundFront distribution, which forms a CDN  
Optionally, a domain name with a TLS certificate can be attached to the CloudFront distribution  

You can also integrate some REST API that could be an API Gateway, Elastic Container, Elastic K8S Service
or any other backend that supports HTTPS and can be accessed by URL

## Managed Resources
List of AWS resources that're managed by this terraform application

- S3 Bucket
- CloudFront OAC(Origin Access Control)
- CloudFront Distribution
- S3 Bucket Policy
- (Optional) Route53 A/CNAME Record